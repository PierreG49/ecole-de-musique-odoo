Le module si présent est utilisable seulement pour la création d'instruments, de cours, d'élèves, de professeurs et d'examens.
Il n'y a aucune liaison entre les classes faute de temps.

Le projet est récupérable en format .zip via Gitlab

Pour initialiser le module sur odoo :
python3 odoo-bin -d projet_ecole_musique -u ecolemusique -c odoo.conf

Module développé par Marin Berthomé et Pierre Griffon
