{
    'name': "Ecole de musique",

    'summary': """Gestion simple d'une ecole de musique""",

    'description': """
        Ce module a pour but de gerer simplement une ecole de musique, il prend en compte :
            Les cours,
            les éléves,
            les profésseurs,
            les instruments,
            les examens
    """,

    'author': "Marin Berthomé et Pierre Griffon",
    'website': "http://www.yourcompany.com",
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
         'security/ir.model.access.csv',
         'views/ecolemusique.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
         'demo/demo.xml'
     ],
}