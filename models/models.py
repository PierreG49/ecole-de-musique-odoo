from odoo import models, fields, api

class Instruments(models.Model):
    _name = 'ecolemusique.instruments'
    _description = "Ecole de musique Instruments"

    name = fields.Char(string="Title", required=True)
    description = fields.Text()

class Eleves(models.Model):
    _name = 'ecolemusique.eleves'
    _prenom = 'ecolemusique.eleves'
    _description = "Ecole de musique eleves"

    name = fields.Char(string="Nom", required=True)
    prenom = fields.Char(string="Prenom", required=True)
    description = fields.Text()

class Professeurs(models.Model):
    _name = 'ecolemusique.professeurs'
    _prenom = 'ecolemusique.professeurs'
    _description ='Ecole de musique professeurs'
    _spécialité = "Ecole de musique professeurs"

    name = fields.Char(string="Nom", required=True)
    prenom = fields.Char(string="Prenom", required=True)
    description = fields.Text()
    spécialité = fields.Text()

class Cours(models.Model):
    _name = 'ecolemusique.cours'
    _description ='Ecole de musique cours description'
    _salle = "Ecole de musique cours salle"
    _horaire = "Ecole de musique cours horaire"

    name = fields.Char(string="Titre", required=True)
    description = fields.Text()
    salle = fields.Text()
    horaire = fields.Text()

class Examens(models.Model):
    _name = 'ecolemusique.examens'
    _description ='Ecole de musique examens description'
    _salle = "Ecole de musique examens salle"
    _horaire = "Ecole de musique examens horaire"

    name = fields.Char(string="Titre", required=True)
    description = fields.Text()
    salle = fields.Text()
    horaire = fields.Text()